package com.synch.synch;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SynchApplicationTests {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Qualifier("reader")
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Before
    public void setUp() throws Exception {
        //jdbcTemplate.setDataSource(dataSource);
      //  H2Builder.build(jdbcTemplate);
    }

    @Test
    public void contextLoads() throws Exception {
        jobLauncherTestUtils.launchJob();
    }


}
