package config;

import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class TestConfig {


    @Bean
    public JobLauncherTestUtils jobLauncherTestUtils()
    {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        return jobLauncherTestUtils;
    }
}
