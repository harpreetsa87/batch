package com.synch.synch.writer;

import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
public class WriterConfig {

    private static final String LOAD_QUERY = "INSERT INTO LOGININFORMATION_MIGRATED VALUES (:auth)";

    @Autowired
    @Qualifier("redisDataSource")
    DataSource redisDataSource;

    @Bean
    public JdbcBatchItemWriter<RedisData> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<RedisData>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql(LOAD_QUERY)
                .dataSource(redisDataSource)
                .build();
    }


}
