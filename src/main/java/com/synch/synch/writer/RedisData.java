package com.synch.synch.writer;

public class RedisData {

    private String auth;

    public RedisData() {
    }

    public RedisData(String auth) {
        this.auth = auth;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
