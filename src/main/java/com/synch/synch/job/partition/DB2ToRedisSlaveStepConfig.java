package com.synch.synch.job.partition;

import com.synch.synch.exception.ItemFailureLoggerListener;
import com.synch.synch.writer.RedisData;
import com.synch.synch.processor.UserInformationItemProcessor;
import com.synch.synch.reader.UserInformation;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DuplicateKeyException;

import javax.batch.api.chunk.listener.SkipWriteListener;

@Configuration
public class DB2ToRedisSlaveStepConfig {

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    ItemReader reader;


    @Autowired
    UserInformationItemProcessor processor;

    @Autowired
    ItemWriteListener skipWriteListener;

    @Value("${db2ToRedis.chunkSize}")
    private Integer chunkSize;

    @Autowired
    JdbcBatchItemWriter writer;

    @Bean(name = "dB2ToRedisSlaveStep")
    public Step slave() {
        return stepBuilderFactory.get("dB2ToRedisSlaveStep").<UserInformation, RedisData>chunk(chunkSize)
                .reader(reader)
                .processor(processor)
                .writer(writer)
               // .faultTolerant().skipLimit(10)
                .listener(skipWriteListener)
                .build();
    }


}
