package com.synch.synch.job.partition;

import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Component
public class RangePartitioner implements Partitioner {

    @Qualifier("db2DataSource")
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${db2ToRedis.partition.query.min.record}")
    private String minRecordQuery;

    @Value("${db2ToRedis.partition.query.max.record}")
    private String maxRecordQuery;

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(RangePartitioner.class);

    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {

        int min = jdbcTemplate.queryForObject(minRecordQuery, Integer.class);
        int max = jdbcTemplate.queryForObject(maxRecordQuery, Integer.class);


        int targetSize = (max - min) / gridSize + 1;
        Map<String, ExecutionContext> result
                = new HashMap<String, ExecutionContext>();
        int number = 0;
        int start = min;
        int end = start + targetSize - 1;
        while (start <= max) {
            ExecutionContext value = new ExecutionContext();
            result.put("partition" + number, value);
            if (end >= max) {
                end = max;
            }
            value.putInt("fromId", start);
            value.putInt("toId", end);
            start += targetSize;
            end += targetSize;
            number++;
        }
        return result;
    }
}
