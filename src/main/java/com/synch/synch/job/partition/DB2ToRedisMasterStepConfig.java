package com.synch.synch.job.partition;

import com.synch.synch.writer.RedisData;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DB2ToRedisMasterStepConfig {

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private PartitionHandler partitionHandler;

    @Autowired
    private RangePartitioner rangePartitioner;

    @Autowired
    private Step redisSlaveStep;

    @Bean(name = "dB2ToRedisMasterStep")
    public Step dB2ToRedisMasterStep(JdbcBatchItemWriter<RedisData> writer) {
        return stepBuilderFactory.get("dB2ToRedisMasterStep")
                .partitioner(redisSlaveStep.getName(), rangePartitioner).partitionHandler(partitionHandler)
                .build();
    }

}
