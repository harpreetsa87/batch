package com.synch.synch.job;

import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobConfig {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(JobConfig.class);

    @Autowired
    public JobBuilderFactory jobBuilderFactory;


    @Autowired
    @Qualifier("dB2ToRedisMasterStep")
    Step dB2ToRedisMasterStep;


    @Bean
    public Job importUserJob() {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(dB2ToRedisMasterStep)
                .end()
                .build();
    }


}
