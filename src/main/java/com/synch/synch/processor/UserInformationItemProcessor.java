package com.synch.synch.processor;

import com.synch.synch.writer.RedisData;
import com.synch.synch.reader.UserInformation;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class UserInformationItemProcessor implements ItemProcessor<UserInformation, RedisData> {


    private static final org.slf4j.Logger log = LoggerFactory.getLogger(UserInformationItemProcessor.class);

    @Override
    public RedisData process(UserInformation userInformation) throws Exception {
        RedisData userInformationProcessed = new RedisData();
        userInformationProcessed.setAuth(userInformation.getUserId() + "$" + userInformation.getPassword());
        return userInformationProcessed;
    }
}
