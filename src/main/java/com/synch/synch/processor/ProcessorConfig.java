package com.synch.synch.processor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProcessorConfig {

    @Bean
    public UserInformationItemProcessor processor() {
        return new UserInformationItemProcessor();
    }

}
