package com.synch.synch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class SynchApplication {
    public static void main(String[] args) {
        SpringApplication.run(SynchApplication.class, args);
    }
}
