package com.synch.synch.reader;

import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


@Configuration
public class ReaderConfig {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ReaderConfig.class);
    private static final String LOAD_QUERY = "SELECT userid,password FROM LOGININFORMATION";


    @Autowired
    @Qualifier("db2DataSource")
    private DataSource db2DataSource;

    @Value("${db2ToRedis.db2.selectquery.clause.select}")
    private String selectClause;

    @Value("${db2ToRedis.db2.selectquery.clause.from}")
    private String fromClause;

    @Value("${db2ToRedis.db2.selectquery.clause.where}")
    private String whereClause;

    @Value("${db2ToRedis.db2.selectquery.clause.sortKey}")
    private String sortKey;

    @Value("${db2ToRedis.db2.selectquery.pagesize}")
    private Integer pageSize;

    @Bean
    @StepScope
    public JdbcPagingItemReader<UserInformation> userInformationReader(
            @Value("#{stepExecutionContext[fromId]}") final String fromId,
            @Value("#{stepExecutionContext[toId]}") final String toId,
            @Value("#{stepExecutionContext[name]}") final String name) {

        log.info("Slave Reader -  " + fromId + " " + toId);

        JdbcPagingItemReader<UserInformation> reader = new JdbcPagingItemReader<>();
        reader.setDataSource(db2DataSource);
        reader.setQueryProvider(queryProvider());
        Map<String, Object> parameterValues = new HashMap<>();
        parameterValues.put("fromId", fromId);
        parameterValues.put("toId", toId);
        reader.setParameterValues(parameterValues);
        reader.setPageSize(pageSize);
        reader.setRowMapper(new BeanPropertyRowMapper<UserInformation>() {{
            setMappedClass(UserInformation.class);
        }});
        return reader;
    }


    @Bean
    public PagingQueryProvider queryProvider() {
        SqlPagingQueryProviderFactoryBean provider = new SqlPagingQueryProviderFactoryBean();
        provider.setDataSource(db2DataSource);
        provider.setSelectClause(selectClause);
        provider.setFromClause(fromClause);
        provider.setWhereClause(whereClause);
        provider.setSortKey(sortKey);
        try {
            return provider.getObject();
        } catch (Exception e) {
            log.info("queryProvider exception ");
            e.printStackTrace();
        }

        return null;
    }


}
