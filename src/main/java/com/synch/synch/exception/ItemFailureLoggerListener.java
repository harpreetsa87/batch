package com.synch.synch.exception;

import com.synch.synch.processor.UserInformationItemProcessor;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.core.annotation.OnSkipInWrite;
import org.springframework.batch.core.listener.ItemListenerSupport;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import javax.batch.api.chunk.listener.SkipWriteListener;
import java.util.List;

@Component
public class ItemFailureLoggerListener implements ItemWriteListener {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ItemFailureLoggerListener.class);


    @Override
    public void beforeWrite(List items) {

    }

    @Override
    public void afterWrite(List items) {

    }

    @Override
    public void onWriteError(Exception exception, List items) {
        log.error(exception.getLocalizedMessage());
    }
}
